docker cp users.json CONTAINER_NAME:/users.json

docker exec -it CONTAINER_NAME  bash

mongoimport --db MY_DB --collection users --drop --jsonArray --batchSize 1 --file ./users.json
